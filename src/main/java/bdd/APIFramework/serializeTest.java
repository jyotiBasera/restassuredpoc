package bdd.APIFramework;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import pojo.AddPlace;
import pojo.Location;

import static io.restassured.RestAssured.*;

import java.util.ArrayList;
import java.util.List;
//This class can be removed from the framework. This is just to understand simple Rest Assured methods and JSON path for Add/Get/Delete Place APIs
public class serializeTest {

	public static void main(String[] args) {
		AddPlace p= new AddPlace();
		p.setAccuracy(50);
		p.setAddress("29 layout, cohen");
		p.setLanguage("Spanish");
		p.setPhone_number("8937652901");
		p.setWebsite("https://rahulshettyacademy.com");
		p.setName("Frontline house");
		List<String> myList = new ArrayList<String>();
		myList.add("shoe park");
		myList.add("shop");
		p.setTypes(myList);
		
		Location l= new Location();
		l.setLat(-38.383494);
		l.setLng(33.427362);
		p.setLocation(l);
		RestAssured.baseURI="https://rahulshettyacademy.com";
		
		//ADD_PLACE
		Response res = given().log().all().queryParam("key", "qaclick123").body(p).
		when().post("/maps/api/place/add/json").then().assertThat().statusCode(200).extract().response();
		String responseString = res.asString();
		System.out.println("Place got added successfully and response is: "+responseString);
		JsonPath js = new JsonPath(responseString);
		String placeId = js.getString("place_id");
		System.out.println("Place Id is: " +placeId);
		
		//GET_PLACE
		String getResponse = given().log().all().queryParam("key", "qaclick123").
		when().get("/maps/api/place/get/json").then().log().all().assertThat().statusCode(200).extract().response().asString();
		System.out.println("Place got fetched successfully and response is: "+getResponse);
		
		//DELETE_PLACE
		String deleteResponse = given().log().all().queryParam("key", "qaclick123").
		body("{\r\n  \"place_id\":\""+placeId+"\"\r\n}").
		when().delete("/maps/api/place/delete/json").then().assertThat().statusCode(200).extract().response().asString();
		System.out.println("Place got deleted successfully and response is: "+deleteResponse);
	}

}
