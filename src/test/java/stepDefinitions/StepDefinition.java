package stepDefinitions;

import static io.restassured.RestAssured.given;
import java.io.IOException;
import static org.junit.Assert.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import resources.APIResources;
import resources.TestDataBuild;
import resources.Utils;

public class StepDefinition extends Utils {
	RequestSpecification res;
	ResponseSpecification resspec;
	Response response;
	TestDataBuild data = new TestDataBuild();
	static String place_id; // all test cases in that particular run will refer to same static set value of
							// that variable else will set to null

	@Given("Add Place Payload with {string}  {string} {string}")
	public void add_Place_Payload_with(String name, String language, String address) throws IOException {

		res = given().spec(requestSpecification()).body(data.addPlacePayLoad(name, language, address));
		System.out.println("Add place payload with " + name + ", " + language + ", " + address);
		 
	}

	@When("user calls {string} with {string} http request")
	public void user_calls_with_http_request(String resource, String method) {
//constructor will be called with value of resource which you pass
		APIResources resourceAPI = APIResources.valueOf(resource);
		System.out.println(resourceAPI.getResource());

		resspec = new ResponseSpecBuilder().expectStatusCode(200).expectContentType(ContentType.JSON).build();

		if (method.equalsIgnoreCase("POST"))
			response = res.when().post(resourceAPI.getResource());
		else if (method.equalsIgnoreCase("GET"))
			response = res.when().get(resourceAPI.getResource());
		System.out.println("user calls " + resource + " with " + method + " http request");

	}

	@Then("the API call got success with status code {int}")
	public void the_API_call_got_success_with_status_code(Integer int1) {
		
		assertEquals(response.getStatusCode(), 200);
		System.out.println("the API call got success with status code " + int1);

	}

	@Then("{string} in response body is {string}")
	public void in_response_body_is(String keyValue, String Expectedvalue) {
		
		assertEquals(getJsonPath(response, keyValue), Expectedvalue);
		System.out.println(keyValue + " in response body is " + Expectedvalue);
	}

	@Then("verify place_Id created maps to {string} using {string}")
	public void verify_place_Id_created_maps_to_using(String expectedName, String resource) throws IOException {

		// requestSpec
		place_id = getJsonPath(response, "place_id");
		res = given().spec(requestSpecification()).queryParam("place_id", place_id);
		user_calls_with_http_request(resource, "GET");
		String actualName = getJsonPath(response, "name");
		assertEquals(actualName, expectedName);
		System.out.println("Verify place_ID created maps to the " + expectedName + " using " + resource);
	}

	@Given("DeletePlace Payload")
	public void deleteplace_Payload() throws IOException {

		res = given().spec(requestSpecification()).body(data.deletePlacePayload(place_id));
		System.out.println("Delete Place payload is provided");
	}

	@Given("User list Payload")
	public void user_list_Payload() throws IOException {

		res = given().spec(
				new RequestSpecBuilder().setBaseUri(getGlobalValue("baseURL2")).addQueryParam("page", "2").build());
	}

}
