package stepDefinitions;

import java.io.IOException;

import io.cucumber.java.Before;

public class Hooks {

	@Before("@DeletePlace")
	public void beforeScenario() throws IOException
	{		//execute this code only when place id is null(when DeletePlace is running independently)
		//write a code that will give you place id
		
		StepDefinition m =new StepDefinition();
		if(StepDefinition.place_id==null)//Static variable, so use className. instead of m.place_id
		{
		
		m.add_Place_Payload_with("Nelson", "German", "Asia");
		m.user_calls_with_http_request("AddPlaceAPI", "POST");
		m.verify_place_Id_created_maps_to_using("Nelson", "getPlaceAPI");
		}
		
		
	}
}
