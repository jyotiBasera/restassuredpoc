package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features",plugin = {"html:target/cucumber-html-reports", "pretty", "json:target/jsonReports/cucumber-report.json","junit:target/cucumber-junit-reports/Cucumber.xml"},
monochrome= true, glue= {"stepDefinitions"} )
//tags= {"@DeletePlace"}

public class TestRunner {
 
}