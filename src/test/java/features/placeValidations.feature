Feature: Validating Place API's
@AddPlace @Regression
Scenario Outline: Verify if Place is being Succesfully added using AddPlaceAPI
	Given Add Place Payload with "<name>"  "<language>" "<address>"
	When user calls "AddPlaceAPI" with "POST" http request
	Then the API call got success with status code 200
	And "status" in response body is "OK"
	And "scope" in response body is "APP"
	And verify place_Id created maps to "<name>" using "getPlaceAPI"
	
Examples:
	|name 	   | language	|address		   |
	|AA11house | English	|Galaxy World cross|
#	|BB22house | German   	|Sea cross center  |

@DeletePlace @Regression
Scenario: Verify if Delete Place functionality is working

	Given DeletePlace Payload
	When user calls "deletePlaceAPI" with "POST" http request
	Then the API call got success with status code 200
	And "status" in response body is "OK"
	 
@ListUsers @Smoke
Scenario: Verify if user list is fetched

	Given User list Payload
	When user calls "GetListOfUsers" with "GET" http request	
	Then the API call got success with status code 200
	Then "page" in response body is "2"
	
	
	
	
	

	
	
	