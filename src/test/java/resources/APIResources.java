package resources;
//enum is special class in java which has collection of constants or  methods
public enum APIResources {
	
	AddPlaceAPI("/maps/api/place/add/json"),//separate each method declaration with comma so that one collection can be formed
	getPlaceAPI("/maps/api/place/get/json"),
	deletePlaceAPI("/maps/api/place/delete/json"),
	GetListOfUsers("/api/users");
	
	private String resource;//global resource name, this will have global access to the class
	
	APIResources(String resource)//constructor with one argument as method has one string argument
	{
		this.resource=resource;//assign this resource to the local keyword resource, this refers to current class variable "resource"
	}
	
	public String getResource()//to return the resource which is loaded with actual value
	{
		return resource;
	}
	

}
